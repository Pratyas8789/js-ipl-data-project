const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    const playMatchObj={};

    results.forEach((playmatch)=>{
        if(typeof(playMatchObj[playmatch.season]) === 'undefined'){
            playMatchObj[playmatch.season] = 0;
        }
        playMatchObj[playmatch.season]++;
    })
    console.log(playMatchObj);
    fs.writeFileSync("../public/output/1-matches-per-year.json",JSON.stringify(playMatchObj,null,2));
  
  });

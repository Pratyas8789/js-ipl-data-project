const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', () => {

        const Id = {}

        for (let i = 0; i < results.length; i++) {
            Id[results[i]['id']] = results[i]['season']
        }

        const newResult = [];

        fs.createReadStream('../data/deliveries.csv')
            .pipe(csv())
            .on('data', (data) => newResult.push(data))
            .on('end', () => {

                const yearBatsmanarray = {}
                for (let i = 0; i < newResult.length; i++) {
                    const deliveryID = newResult[i]['match_id'];
                    if (Id.hasOwnProperty(deliveryID)) {
                        const year = Id[deliveryID];
                        const batsman = newResult[i]['batsman'];
                        const runs = parseInt(newResult[i]['batsman_runs'])
                        if (!yearBatsmanarray.hasOwnProperty(year)) {
                            let temp = {}
                            let tempA = new Array(2)

                            tempA[0] = parseInt(runs)
                            tempA[1] = 1
                            temp[batsman] = tempA;
                            yearBatsmanarray[year] = temp
                        }
                        else {
                            let temp = yearBatsmanarray[year]
                            let tempA = new Array(2)
                            if (!temp.hasOwnProperty(batsman)) {
                                tempA[0] = runs;
                                tempA[1] = parseInt(1);

                                temp[batsman] = tempA;
                            }
                            else {
                                const r = temp[batsman][0] + runs;
                                const b = temp[batsman][1] + 1;
                                tempA[0] = r;
                                tempA[1] = b;

                                temp[batsman] = tempA;
                            }
                            yearBatsmanarray[year] = temp;


                        }

                    }

                }

                const strikeRatePerPlayer = {}

                for (key in yearBatsmanarray) {
                    let yearBatsmanarrayOfValue = yearBatsmanarray[key]
                    const nameAndStrike = {}
                    for (let key2 in yearBatsmanarrayOfValue) {
                        let strike = (yearBatsmanarrayOfValue[key2][0] * 100 / yearBatsmanarrayOfValue[key2][1])
                        nameAndStrike[key2] = strike.toFixed(2)
                    }
                    strikeRatePerPlayer[key] = nameAndStrike
                }
                fs.writeFileSync("../public/output/7-strike-rate-of-batsMan-each-session.json", JSON.stringify(strikeRatePerPlayer, null, 2));

            });
    });
const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    const yearWiseTeamWon={};
    for(let i=0;i<results.length;i++){
        data=results[i];
        season=data['season'];
        winner=data['winner']
        if(yearWiseTeamWon[season]){
            let obj=yearWiseTeamWon[season];
            if(obj[winner]){
                obj[winner]++;
            }else{
                obj[winner]=1
            }

        }
        else{
            let teamWon={}
            teamWon[winner]=1
            yearWiseTeamWon[season]=teamWon;
        }
    }
    fs.writeFileSync("../public/output/2-matches-won-per-team-per-year.json",JSON.stringify(yearWiseTeamWon,null,2));
    
    
  });
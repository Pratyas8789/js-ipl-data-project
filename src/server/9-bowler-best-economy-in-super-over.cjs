const csv = require('csv-parser')
const fs = require('fs')
const deliveryData = [];

fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => deliveryData.push(data))
    .on('end', () => {
        const bowlerWithEconomy = {}

        for (let index = 0; index < deliveryData.length; index++) {         
            if (deliveryData[index]['is_super_over'] !== '0') {                
                if (typeof(bowlerWithEconomy[deliveryData[index]['bowler']]) === 'undefined') {
                    const arr=[]
                    arr[0]= parseInt(deliveryData[index]['total_runs'])
                    arr[1]= 1
                    bowlerWithEconomy[deliveryData[index]['bowler']] = arr;
                }else{
                    bowlerWithEconomy[deliveryData[index]['bowler']][0]+=parseInt(deliveryData[index]['total_runs'])
                    bowlerWithEconomy[deliveryData[index]['bowler']][1]++
                }
            }
        }
        let max=0;
        let name=""
        for(let key in bowlerWithEconomy){
            let economy=+(bowlerWithEconomy[key][0]/(bowlerWithEconomy[key][1]/6)).toFixed(2);
            bowlerWithEconomy[key]=economy
            if(max<economy){
                max=economy
                name=key
            }
        }
        const bestbowlerWithEconomy={}
        bestbowlerWithEconomy[name]=max
        fs.writeFileSync("../public/output/9-bowler-best-economy-in-super-over.json", JSON.stringify(bestbowlerWithEconomy, null, 2));


    });

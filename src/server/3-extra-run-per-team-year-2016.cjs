const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', () => {
        const Id = [];
        for (let i = 0; i < results.length; i++) {
            if (results[i]['season'] == '2016') {
                Id.push(results[i]['id'])
            }

        }
        const newresults = []
        const finalresult = {}
        fs.createReadStream('../data/deliveries.csv')
            .pipe(csv())
            .on('data', (data) => newresults.push(data))
            .on('end', () => {

                for (let i = 0; i < Id.length; i++) {
                    for (let j = 0; j < newresults.length; j++) {
                        if (newresults[j]['extra_runs'] > 0) {
                            if (Id[i] == newresults[j]['match_id']) {
                                if (finalresult[newresults[j]['bowling_team']]) {
                                    (finalresult[newresults[j]['bowling_team']]) += (+newresults[j]['extra_runs'])
                                } else {
                                    finalresult[newresults[j]['bowling_team']] = (+newresults[j]['extra_runs'])
                                }
                            }
                        }
                    }
                }
                fs.writeFileSync("../public/output/3-extra-run-per-team-year-2016.json", JSON.stringify(finalresult, null, 2));


            });

    });



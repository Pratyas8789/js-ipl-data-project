const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    const yearWisePlayerOfMatchEachSeason={};
    
    for(let i=0;i<results.length;i++){
        data=results[i];
        season=data['season'];
        playerOfMatch=data['player_of_match']
        if(yearWisePlayerOfMatchEachSeason[season]){
            let obj=yearWisePlayerOfMatchEachSeason[season];
            if(obj[playerOfMatch]){
                obj[playerOfMatch]++;
            }else{
                obj[playerOfMatch]=1
            }

        }
        else{
            let teamWon={}
            teamWon[playerOfMatch]=1
            yearWisePlayerOfMatchEachSeason[season]=teamWon;
        }
    }
    const yearWiseHighestPlayerOfTheMatch={}

    for(let key1 in yearWisePlayerOfMatchEachSeason){
      const obj=yearWisePlayerOfMatchEachSeason[key1];
      let max=0;
      for(let key2 in obj){
        const times=obj[key2]
        if(max<=times){
          max=times;
        }
        
      }
      const newobj={}
      for(let key3 in obj){
        if(obj[key3]==max){
          newobj[key3]=max;
        }
      }
      yearWiseHighestPlayerOfTheMatch[key1]=newobj
      
    }
    fs.writeFileSync("../public/output/6-year-wise-playerOfMatch-each-season.json",JSON.stringify(yearWiseHighestPlayerOfTheMatch,null,2));
    
  });
const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', () => {


        const dismisByAnotherPlayer = {}
        for (let i = 0; i < results.length; i++) {
            if (results[i]['player_dismissed'] !== '') {



                if (dismisByAnotherPlayer.hasOwnProperty(results[i]['player_dismissed'])) {
                    const valueobj = dismisByAnotherPlayer[results[i]['player_dismissed']]


                    if (valueobj.hasOwnProperty(results[i]['bowler'])) {
                        valueobj[results[i]['bowler']]++
                    } else {
                        valueobj[results[i]['bowler']] = 1
                    }



                } else {

                    dismisByAnotherPlayer[results[i]['player_dismissed']] = {}


                    dismisByAnotherPlayer[results[i]['player_dismissed']][results[i]['bowler']] = 1

                }


            }

        }
        fs.writeFileSync("../public/output/8-heighest-num-of-one-player-dismiss-by-another-player.json", JSON.stringify(dismisByAnotherPlayer, null, 2));

    });
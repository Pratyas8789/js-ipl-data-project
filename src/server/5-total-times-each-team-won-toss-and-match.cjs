const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', () => {
        const tossAndMatchWinner = {};

        for (let i = 0; i < results.length; i++) {
            if (results[i]['toss_winner'] == results[i]['winner'] ) {
                if(tossAndMatchWinner[results[i]['toss_winner']]){
                    tossAndMatchWinner[results[i]['toss_winner']]++
                }else{
                    tossAndMatchWinner[results[i]['toss_winner']]=1
                }
            }
        }        

        fs.writeFileSync("../public/output/5-total-times-each-team-won-toss-and-match.json",JSON.stringify(tossAndMatchWinner,null,2));

    });



const csv = require('csv-parser')
const fs = require('fs')
const matchesData = [];

fs.createReadStream('../data/matches.csv')
    .pipe(csv())
    .on('data', (data) => matchesData.push(data))
    .on('end', () => {

        const Id = [];

        for (let i = 0; i < matchesData.length; i++) {
            if (matchesData[i]['season'] == 2015) {
                Id.push(matchesData[i]['id'])
            }
        }

        const deliveriesdata = []
        fs.createReadStream('../data/deliveries.csv')
            .pipe(csv())
            .on('data', (data) => deliveriesdata.push(data))
            .on('end', () => {

                const bowlers = []; //all bowlers present
                for (let i = 0; i < Id.length; i++) { //i=id
                    for (let j = 0; j < deliveriesdata.length; j++) {
                        if (deliveriesdata[j]['match_id'] === Id[i]) {
                            if (!bowlers.includes(deliveriesdata[j]['bowler'])) bowlers.push(deliveriesdata[j]['bowler']);
                        }
                    }
                }

                const ballerAndItsEconomicBowlers = {}

                for (let index = 0; index < bowlers.length; index++) {
                    let totalRuns = 0;
                    let totalOvers = 0;
                    for (let j = 0; j < deliveriesdata.length; j++) {
                        if (deliveriesdata[j]['bowler'] === bowlers[index]) {
                            totalRuns += parseInt(deliveriesdata[j]['total_runs'])
                            if (deliveriesdata[j]['ball'] === '6') totalOvers++;
                        }

                    }
                    const economicRate = +(totalRuns / totalOvers).toFixed(2)

                    ballerAndItsEconomicBowlers[bowlers[index]] = economicRate;
                }

                let sortable=[];
                for(var key in ballerAndItsEconomicBowlers){
                    sortable.push([key, ballerAndItsEconomicBowlers[key]]);
                }
                sortable.sort(function (a, b) {
                    return b[1]-a[1];
                })
                

                const finalobj={}

                for(let a=0;a<10;a++){
                    finalobj[sortable[a][0]]=sortable[a][1];
                }

                fs.writeFileSync("../public/output/4-economical-bowlers-in-2015.json", JSON.stringify(finalobj, null, 2));
            });

    });